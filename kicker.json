{
  "name": "Ansible",
  "description": "Provision your infrastructure and deploy your application with [Ansible](https://www.ansible.com/)",
  "template_path": "templates/gitlab-ci-ansible.yml",
  "kind": "hosting",
  "prefix": "ansible",
  "is_component": true,
  "variables": [
    {
      "name": "ANSIBLE_IMAGE",
      "description": "The Docker image used to run Ansible. The image may contain your Ansible sources. **set the version required by your project**",
      "default": "registry.hub.docker.com/cytopia/ansible:latest-tools"
    },
    {
      "name": "ANSIBLE_PROJECT_DIR",
      "description": "Ansible project root directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "ANSIBLE_BASE_APP_NAME",
      "description": "Base application name",
      "default": "$CI_PROJECT_NAME",
      "advanced": true
    },
    {
      "name": "ANSIBLE_ENVIRONMENT_URL",
      "type": "url",
      "description": "The default environments url _(only define for static environment URLs declaration)_\n\n_supports late variable expansion (ex: `https://%{environment_name}.acme.com`)_"
    },
    {
      "name": "ANSIBLE_VAULT_PASSWORD",
      "description": "The Ansible vault password used to decrypt vars",
      "secret": true
    },
    {
      "name": "ANSIBLE_PRIVATE_KEY",
      "description": "The Ansible SSH private key to use in all stages (can be overridden per env)",
      "secret": true
    },
    {
      "name": "ANSIBLE_PUBLIC_KEY",
      "description": "The Ansible SSH public key associated to the private key to be use in all stages (can be overridden per env)",
      "advanced": true
    },
    {
      "name": "ANSIBLE_DEFAULT_INVENTORY",
      "description": "The default inventory, if used"
    },
    {
      "name": "ANSIBLE_DEFAULT_TAGS",
      "description": "The default tags, if used"
    },
    {
      "name": "ANSIBLE_DEFAULT_EXTRA_ARGS",
      "description": "Optional default args to add to the ansible-playbook command line",
      "advanced": true
    },
    {
      "name": "ANSIBLE_FORCE_COLOR",
      "description": "Forces color on Ansible output",
      "type": "boolean",
      "default": "true",
      "advanced": true
    },
    {
      "name": "ANSIBLE_REQUIREMENTS_FILE",
      "description": "The file used to install roles with `ansible-galaxy role install`",
      "default": "requirements.yml",
      "advanced": true
    },
    {
      "name": "ANSIBLE_GALAXY_EXTRA_ARGS",
      "description": "`ansible-galaxy role install` command [extra options](https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html#role-install)",
      "advanced": true
    },
    {
      "name": "ANSIBLE_SCRIPTS_DIR",
      "description": "The Ansible scripts base directory (relative to `$ANSIBLE_PROJECT_DIR`)",
      "default": ".",
      "advanced": true
    },
    {
      "name": "ANSIBLE_HOST_KEY_CHECKING",
      "description": "Enable or disable the SSH host key checking",
      "type": "boolean",
      "default": "false",
      "advanced": true
    },
    {
      "name": "ANSIBLE_DEFAULT_ROLES_PATH",
      "description": "The default path where the roles should be installed",
      "default": "$CI_PROJECT_DIR/roles",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "lint",
      "name": "Ansible Lint",
      "description": "Static code analysis of your Ansible scripts with [Ansible Lint](https://docs.ansible.com/ansible-lint/)",
      "disable_with": "ANSIBLE_LINT_DISABLED",
      "variables": [
        {
          "name": "ANSIBLE_LINT_IMAGE",
          "description": "The Docker image used to run Ansible Lint.",
          "default": "registry.hub.docker.com/haxorof/ansible-lint:latest"
        }
      ]
    },
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "variables": [
        {
          "name": "ANSIBLE_REVIEW_APP_NAME",
          "description": "The application name for review env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_REVIEW_ENVIRONMENT_URL",
          "type": "url",
          "description": "The review environments url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "ANSIBLE_REVIEW_INVENTORY",
          "description": "The inventory for `review` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_REVIEW_TAGS",
          "description": "The tags for `review` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_REVIEW_CLEANUP_TAGS",
          "description": "The tags to cleanup the `review` env",
          "mandatory": true
        },
        {
          "name": "ANSIBLE_REVIEW_EXTRA_ARGS",
          "description": "The command line extra args for `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_REVIEW_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop `review` environments",
          "default": "4 hours"
        },
        {
          "name": "ANSIBLE_REVIEW_PLAYBOOK_FILE",
          "description": "The playbook filename for `review` env",
          "mandatory": true
        },
        {
          "name": "ANSIBLE_REVIEW_CLEANUP_PLAYBOOK_FILE",
          "description": "The playbook filename to cleanup `review` env (only define if different from deployment playbook)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_REVIEW_PRIVATE_KEY",
          "description": "The SSH private key to be use in `review` env (only define to override default)",
          "secret": true
        },
        {
          "name": "ANSIBLE_REVIEW_PUBLIC_KEY",
          "description": "The SSH public key associated to the private key to be use in `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_REVIEW_VAULT_PASSWORD",
          "description": "The Ansible vault password for `review` env (only define to override default)",
          "secret": true,
          "advanced": true
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "variables": [
        {
          "name": "ANSIBLE_INTEG_APP_NAME",
          "description": "The application name for integration env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_INTEG_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop the `integration` env",
          "default": "never"
        },
        {
          "name": "ANSIBLE_INTEG_ENVIRONMENT_URL",
          "type": "url",
          "description": "The integration environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "ANSIBLE_INTEG_INVENTORY",
          "description": "The inventory for `integration` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_INTEG_TAGS",
          "description": "The tags for `integration` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_INTEG_CLEANUP_TAGS",
          "description": "The tags to cleanup the `integration` env",
          "mandatory": true
        },
        {
          "name": "ANSIBLE_INTEG_EXTRA_ARGS",
          "description": "The command line extra args for `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_INTEG_PLAYBOOK_FILE",
          "description": "The playbook filename for `integration` env",
          "mandatory": true
        },
        {
          "name": "ANSIBLE_INTEG_CLEANUP_PLAYBOOK_FILE",
          "description": "The playbook filename to cleanup `integration` env (only define if different from deployment playbook)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_INTEG_PRIVATE_KEY",
          "description": "The SSH private key to be use in `integration` env (only define to override default)",
          "secret": true,
          "advanced": true
        },
        {
          "name": "ANSIBLE_INTEG_PUBLIC_KEY",
          "description": "The SSH public key associated to the private key to be use in `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_INTEG_VAULT_PASSWORD",
          "description": "The Ansible vault password for `integration` env (only define to override default)",
          "secret": true,
          "advanced": true
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`main` or `master` by default)",
      "variables": [
        {
          "name": "ANSIBLE_STAGING_APP_NAME",
          "description": "The application name for staging env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_STAGING_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop the `staging` env",
          "default": "never"
        },
        {
          "name": "ANSIBLE_STAGING_ENVIRONMENT_URL",
          "type": "url",
          "description": "The staging environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "ANSIBLE_STAGING_INVENTORY",
          "description": "The inventory for `staging` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_STAGING_TAGS",
          "description": "The tags for `staging` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_STAGING_CLEANUP_TAGS",
          "description": "The tags to cleanup the `staging` env",
          "mandatory": true
        },
        {
          "name": "ANSIBLE_STAGING_EXTRA_ARGS",
          "description": "The command line extra args for `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_STAGING_PLAYBOOK_FILE",
          "description": "The playbook filename for `staging` env",
          "mandatory": true
        },
        {
          "name": "ANSIBLE_STAGING_CLEANUP_PLAYBOOK_FILE",
          "description": "The playbook filename to cleanup `staging` env (only define if different from deployment playbook)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_STAGING_PRIVATE_KEY",
          "description": "The SSH private key to be use in `staging` env (only define to override default)",
          "secret": true,
          "advanced": true
        },
        {
          "name": "ANSIBLE_STAGING_PUBLIC_KEY",
          "description": "The SSH public key associated to the private key to be use in `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_STAGING_VAULT_PASSWORD",
          "description": "The Ansible vault password for `staging` env (only define to override default)",
          "secret": true,
          "advanced": true
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "variables": [
        {
          "name": "ANSIBLE_PROD_APP_NAME",
          "description": "The application name for production env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_PROD_ENVIRONMENT_URL",
          "type": "url",
          "description": "The production environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "ANSIBLE_PROD_DEPLOY_STRATEGY",
          "description": "Defines the deployment to production strategy.",
          "type": "enum",
          "values": ["manual", "auto"],
          "default": "manual"
        },
        {
          "name": "ANSIBLE_PROD_INVENTORY",
          "description": "The inventory for `production` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_PROD_TAGS",
          "description": "The tags for `production` env (only define to override default)"
        },
        {
          "name": "ANSIBLE_PROD_EXTRA_ARGS",
          "description": "The command line extra args for `production` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_PROD_PLAYBOOK_FILE",
          "description": "The playbook filename for `production` env",
          "mandatory": true
        },
        {
          "name": "ANSIBLE_PROD_PRIVATE_KEY",
          "description": "The SSH private key to be use in `production` env (only define to override default)",
          "secret": true,
          "advanced": true
        },
        {
          "name": "ANSIBLE_PROD_PUBLIC_KEY",
          "description": "The SSH public key associated to the private key to be use in `production` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "ANSIBLE_PROD_VAULT_PASSWORD",
          "description": "The Ansible vault password for `production` env (only define to override default)",
          "secret": true,
          "advanced": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-ansible-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
